import UIKit

var str = "Hello, playground"

//задание 2
let x = 5
let y = 4
print( x + y)

/// или
func findSum (){
    let x = 5
    let y = 4
    print( x + y)
}
findSum()

//задание 3
let someName: String = "Igor"
var someStory: String = "My name is \(someName), I am a student"
print (someStory)


